function Template(template, data) {
    this.pattern = /\{\{\s*.+\s*\}\}/g;
    this.variable = /^\{\{\s*|\s*\}\}$/g;
    this.parts = template.split(this.pattern);
    this.matches = template.match(this.pattern);
    this.stream = this.parts[0];
    this.template = this.parse(data);
    this.render();
}

Template.prototype = {
    render: function () {
        document.body.insertAdjacentHTML('afterbegin', this.template);
    },
    parse: function (obj) {
        if (!this.matches) return;

        for (var i = 0; i < this.matches.length; i++) {
            var match = this.matches[i];
            var isIf = / if /.test(match);
            var str = '';

            if (isIf) {
                var operands = match.replace(this.variable, '').split('if');
                var value = operands[0].replace(/'/g, '');
                var expr = operands[1].split(/!?=+/);
                var operator = operands[1].match(/(\!?\=+)/)[1];
                var el = expr[0].trim().replace(/data\./, '');
                var bool = expr[1].trim() === 'true';
                var compared = this.deeps(obj, el) === bool;
                str += compared ? value : '';
            } else {
                var el = match.replace(this.variable, '').replace(/data\./, '');
                str += this.deeps(obj, el);
            }

            this.stream += str + this.parts[i + 1];
        }

        return this.stream;
    },
    deeps: function (obj, val) {
        var hs = val.split('.');
        var deep;

        for (var i = 0; i < hs.length; i++) {
            deep = deep ? deep[hs[i]] : obj[hs[i]];
        }

        return deep;
    }
};